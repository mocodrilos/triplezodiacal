var loadState = {

    preload: function() {
        var loadingLabel = game.add.text(80, 150, 'loading...', {font: '30px Courier', fill: '#fff'});

        this.numSignos = 12;
        this.rapidez = 12;

        //Imagenes, Sprisheets, Tilesets, Animaciones...

        game.load.spritesheet('signos', 'assets/signos/SignosSpriteSheet.png', 800, 720, this.numSignos);
        game.load.spritesheet('Ads', 'assets/img/ads.png', 256, 128, 4);
        game.load.spritesheet('rasp_anim', 'assets/img/raspar_anim.png', 484, 170, 4);
        game.load.spritesheet('rasp_shadow', 'assets/img/raspar_shadow.png', 484, 170, 4);
        game.load.image('logoTriple', 'assets/img/TripleZodiacal.png');
        game.load.image('sagitario', 'assets/signos/Sagitario.png');
        game.load.image('scorpio', 'assets/signos/Escorpion.png');
        game.load.image('tauro', 'assets/signos/Tauro.png');
        game.load.image('virgo', 'assets/signos/Virgo.png');
        game.load.image('leo', 'assets/signos/Leo.png');
        game.load.image('libra', 'assets/signos/Libra.png');
        game.load.image('geminis', 'assets/signos/Geminis.png');
        game.load.image('piscis', 'assets/signos/Piscis.png');
        game.load.image('aries', 'assets/signos/Aries.png');
        game.load.image('cancer', 'assets/signos/Cancer.png');
        game.load.image('capricornio', 'assets/signos/Capricornio.png');
        game.load.image('acuario', 'assets/signos/Acuario.png');
        game.load.image('carton', 'assets/img/carton.png');
        game.load.image('b_jugar', 'assets/img/b_jugar.png');
        game.load.image('b_ok', 'assets/img/b_ok.png');
        game.load.image('machine', 'assets/img/machine.png');
        game.load.image('bsf', 'assets/img/bsf.png');
        game.load.image('boton', 'assets/img/boton.png');
        game.load.image('dedo', 'assets/img/dedo.png');
        game.load.image('herradura', 'assets/img/herradura.png');
        game.load.image('raspar', 'assets/img/raspar.png');
        game.load.image('b_jugarPress', 'assets/img/b_jugarPress.png');
        game.load.image('b1U', 'assets/img/b1up.png');
        game.load.image('b1D', 'assets/img/b1down.png');
        game.load.image('b2U', 'assets/img/b2up.png');
        game.load.image('b2D', 'assets/img/b2down.png');
        game.load.image('b3U', 'assets/img/b3up.png');
        game.load.image('b3D', 'assets/img/b3down.png');
        game.load.image('b4U', 'assets/img/b4up.png');
        game.load.image('b4D', 'assets/img/b4down.png');
        game.load.image('aviso', 'assets/img/aviso.png');
        game.load.image('gana', 'assets/img/gana.png');
        game.load.image('m1', 'assets/img/m1.png');
        game.load.image('m2', 'assets/img/m2.png');
        game.load.image('m3', 'assets/img/m3.png');
        game.load.image('m4', 'assets/img/m4.png');
        game.load.image('popup', 'assets/img/PopUp.png');
        game.load.image('pot', 'assets/img/pot.png');

        //Sonidos, EFx, Musica...
        game.load.audio('efx1', 'assets/audio/efx_coin.wav');
        game.load.audio('efx2', 'assets/audio/efx_coin2.wav');
        game.load.audio('efx3', 'assets/audio/efx_rasp.wav');
        game.load.audio('track', 'assets/audio/track1.mp3');
    },

    create: function() {

        game.stage.setBackgroundColor('#000');
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.EXACT_FIT;
        game.state.start('stand');
    }
};
