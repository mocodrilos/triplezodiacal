var game;

var playState = {

    preload: function(){
        this.numSignos = 12;
        this.rapidez = 24;

        if(game.cache.getJSON('ticket') == null )

            game.load.json('ticket', 'assets/data/ticket.json');

        else{

            var ticket = game.cache.getJSON('ticket');
            game.load.json('ticket', 'assets/data/ticket.json'+ticket.serial);

        }

    },

    create: function(){

        music = game.add.audio('track');
        music.volume = 0.4;
        //music.play();

        efx_push = game.add.audio('efx2');
        efx_push.volume = 0.2;

        var machine = game.add.image(-20, 10, 'machine');
        machine.scale.setTo(1);

        var logo = game.add.image(280, 177, 'logoTriple');
        logo.anchor.set(0.5);
        logo.scale.setTo(0.4);

        var spaKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);

        var aviso = game.add.image(game.world.x+640, game.world.y+100, 'aviso');
        var rules = game.add.text(aviso.x+187, aviso.y+40, 'Reglas');
        rules.anchor.set(0.5);
        rules.align = 'center';
        rules.font = 'Arial Black';
        rules.fontSize = 30;
        rules.fontWeight = 'bold';
        rules.stroke = '#222';
        rules.strokeThickness = 6;
        rules.fill = '#64ff57';

        var rule1 = game.add.text(aviso.x+20, aviso.y+90, '1.-  Presiona [Jugar] para Girar la Ruleta');
        rule1.align = 'justify';
        rule1.font = 'Arial';
        rule1.fontSize = 16;
        rule1.stroke = '#222';
        rule1.strokeThickness = 5;
        rule1.fill = '#16b2bf';

        var rule2 = game.add.text(aviso.x+20, aviso.y+140, '2.-  Decide cuando detener el Giro \npresionando nuevamente [Jugar]');
        rule2.align = 'justify';
        rule2.font = 'Arial';
        rule2.fontSize = 16;
        rule2.stroke = '#222';
        rule2.strokeThickness = 5;
        rule2.fill = '#16b2bf';

        var rule3 = game.add.text(aviso.x+20, aviso.y+220, '3.-  Raspa y Gana!');
        rule3.align = 'justify';
        rule3.font = 'Arial';
        rule3.fontSize = 16;
        rule3.stroke = '#222';
        rule3.strokeThickness = 5;
        rule3.fill = '#16b2bf';

        var bjugar = game.add.image(game.world.x+450, game.world.y+520, 'b_jugar');
        var b1 = game.add.image(game.world.x+100, game.world.y+520, 'b1U');
        var b2 = game.add.image(game.world.x+170, game.world.y+520, 'b2U');
        var b3 = game.add.image(game.world.x+240, game.world.y+520, 'b3U');
        var b4 = game.add.image(game.world.x+310, game.world.y+520, 'b4U');

        this.barraAnterior;
        this.barraActual = game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR);

        this.signos1 = game.add.sprite(80, 300, 'signos', 0);
        this.signos1.scale.setTo(.17);
        var cuadroAleatorio = Math.floor(Math.random() * 12);
        this.signos1.frame = cuadroAleatorio;
        this.signos1.cuadro = cuadroAleatorio;
        this.isJugando = false;

        this.signos2 = game.add.sprite(244, 300, 'signos', 0);
        this.signos2.scale.setTo(.17);
        var cuadroAleatorio = Math.floor(Math.random() * 12);
        this.signos2.frame = cuadroAleatorio;
        this.signos2.cuadro = cuadroAleatorio;

        this.signos3 = game.add.sprite(410, 300, 'signos', 0);
        this.signos3.scale.setTo(.17);
        var cuadroAleatorio = Math.floor(Math.random() * 12);
        this.signos3.frame = cuadroAleatorio;
        this.signos3.cuadro = cuadroAleatorio;

        var ticket = game.cache.getJSON('ticket');
        var ntictxt = game.add.text(320, game.world.y+30, "Nro. Jugadas: " + ticket.numTicket, { fill: '#ffffff' });

        var stateTi = ticket.state;

        game.add.text(50, game.world.y+30, ticket.serial, { fill: '#fff'});

        var ads = game.add.image(720, 420, 'Ads');
        ads.scale.setTo(0.9);
        ads.animations.add('play');
        ads.animations.play('play', 0.5, true);

    },

    update: function(){

        this.barraAnterior = this.barraActual;
        this.barraActual = game.input.keyboard.isDown(Phaser.Keyboard.SPACEBAR);

        if (this.barraActual && !this.barraAnterior) {
            if (this.isJugando)
                this.detener();
            else
                this.comenzar();
        }

        if (!this.isJugando){
            return;
        }

        //Ciclo de Random en las imagenes...
        this.signos1.cuadro += this.rapidez * game.time.physicsElapsed;

        if (this.signos1.cuadro >= this.numSignos)
            this.signos1.cuadro -= this.numSignos;
        this.signos1.frame = Math.floor(this.signos1.cuadro);

        //Segundo Signo
        this.signos2.cuadro += this.rapidez * game.time.physicsElapsed;

        if (this.signos2.cuadro >= this.numSignos)
            this.signos2.cuadro -= this.numSignos;
        this.signos2.frame = Math.floor(this.signos2.cuadro);

        //Tercer Signo
        this.signos3.cuadro += this.rapidez * game.time.physicsElapsed;

        if (this.signos3.cuadro >= this.numSignos)
            this.signos3.cuadro -= this.numSignos;
        this.signos3.frame = Math.floor(this.signos3.cuadro);
        //Termina Ciclo de Random...
    },

    comenzar: function () {

        if (this.isJugando)
            return;

        game.add.image(game.world.x+450, game.world.y+520, 'b_jugarPress');
        efx_push.play();

        this.isJugando = true;
    },

    detener: function () {

        //this.isJugando = false;
        efx_push.play();
        game.state.start('raffle');
    }
};
