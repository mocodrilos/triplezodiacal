var standState = {

    create: function(){

        efx_coin = game.add.audio('efx1');
        efx_coin.volume = 0.2;

        //Debugger Phaser Activo
        game.add.plugin(Phaser.Plugin.Debug);

        var gana = game.add.image(game.world.x+800, game.world.y+150, 'gana');
        gana.anchor.set(.5);

        this.logoTriple = game.add.image(game.world.centerX, 256, 'logoTriple');
        this.logoTriple.anchor.set(.5);

        //var ticket = game.cache.getJSON('ticket');
        //var sertxt = game.add.text(game.world.centerX+120, game.world.centerX-250, "Serial: " + ticket.serial, { fill: '#ffffff' });

        var presBoton = game.add.text(game.world.centerX, game.world.height-80, 'Presiona [JUGAR] para iniciar');
        presBoton.anchor.set(0.5);
        presBoton.align = 'center';
        presBoton.font = 'Arial Black';
        presBoton.fontSize = 30;
        presBoton.fontWeight = 'bold';
        presBoton.stroke = '#222';
        presBoton.strokeThickness = 6;
        presBoton.fill = '#64ff57';

        var jugarKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        jugarKey.onDown.addOnce(this.start, this);
    },

    start: function(){

        efx_coin.play();
        game.state.start('play');
    },
};
