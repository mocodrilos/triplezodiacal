//Inicializacion de Variables Globales.
var key1,
    key2,
    key3,
    key4,
    tRandom,
    count,
    amTime,
    sigTime,
    winTime,
    ticket,
    stateTi,
    leveTi;
//Inicializacion de estado Raffle.
var raffleState = {

    create: function(){

        ticket = game.cache.getJSON('ticket'); //Asignacion de ticket.
        ticket.numTicket--; //Decrementar Numero de tickets.
        amTime = 200; //Tiempo de Animacion.
        sigTime = 100; //Tiempo de Signos.
        winTime = 300; //Tiempo de Victoria.

        game.load.json('ticket', ticket); //Cargar el ticket via JSON.

        game.add.text(320, game.world.y+30, "Nro. Jugadas: " + ticket.numTicket, { fill: '#fff' });
        game.add.text(50, game.world.y+30, ticket.serial, { fill: '#fff'});
        leveTi = ticket.level; //Asignar variable a atributo del ticket.
        //Verificar el nivel de Premio
        if(leveTi == 0)
            console.log('Ticket sin Premios');
        if(leveTi == 1)
            console.log('Ticket con premio Menor');
        if(leveTi == 2)
           console.log('Ticket con premio Mayor');

        this.machine = game.add.image(-20, 10, 'machine');
        var raspar = game.add.image(this.machine.x+90, this.machine.y+270, 'raspar');

        var logo = game.add.image(280, 177, 'logoTriple');
        logo.anchor.set(0.5);
        logo.scale.setTo(0.4);

        var aviso = game.add.image(game.world.x+640, game.world.y+100, 'aviso');
        var rules = game.add.text(aviso.x+187, aviso.y+40, 'Reglas');
        rules.anchor.set(0.5);
        rules.align = 'center';
        rules.font = 'Arial Black';
        rules.fontSize = 30;
        rules.fontWeight = 'bold';
        rules.stroke = '#222';
        rules.strokeThickness = 6;
        rules.fill = '#64ff57';

        var rule4 = game.add.text(aviso.x+20, aviso.y+90, '4.-  Selecciona un Amuleto de la Suerte.');
        rule4.align = 'justify';
        rule4.font = 'Arial';
        rule4.fontSize = 16;
        rule4.stroke = '#222';
        rule4.strokeThickness = 5;
        rule4.fill = '#16b2bf';

        var m1 = game.add.image(aviso.x+20, aviso.y+146, 'bsf');
        m1.scale.setTo(.06);
        var txt1 = game.add.text(m1.x+40, m1.y+90, 'Num 1');
        txt1.anchor.set(.5);
        txt1.fontSize = 12;
        txt1.fill = 'blue';

        var m2 = game.add.image(aviso.x+106, aviso.y+146, 'boton');
        m2.scale.setTo(.06);
        var txt2 = game.add.text(m2.x+40, m2.y+90, 'Num 2');
        txt2.anchor.set(.5);
        txt2.fontSize = 12;
        txt2.fill = 'blue';

        var m3 = game.add.image(aviso.x+192, aviso.y+146, 'dedo');
        m3.scale.setTo(.27);
        var txt3 = game.add.text(m3.x+40, m3.y+90, 'Num 3');
        txt3.anchor.set(.5);
        txt3.fontSize = 12;
        txt3.fill = 'blue';

        var m4 = game.add.image(aviso.x+288, aviso.y+146, 'herradura');
        m4.scale.setTo(.1);
        var txt4 = game.add.text(m4.x+40, m4.y+90, 'Num 4');
        txt4.anchor.set(.5);
        txt4.fontSize = 12;
        txt4.fill = 'blue';

        var bjugar = game.add.image(game.world.x+450, game.world.y+520, 'b_jugar');
        var b1 = game.add.image(game.world.x+100, game.world.y+520, 'b1U');
        var b2 = game.add.image(game.world.x+170, game.world.y+520, 'b2U');
        var b3 = game.add.image(game.world.x+240, game.world.y+520, 'b3U');
        var b4 = game.add.image(game.world.x+310, game.world.y+520, 'b4U');

        var ads = game.add.image(720, 420, 'Ads');
        ads.scale.setTo(0.9);
        ads.animations.add('play');
        ads.animations.play('play', 0.5, true);

        key1 = game.input.keyboard.addKey(Phaser.Keyboard.ONE);
        key1.onDown.addOnce(this.action1, this);

        key2 = game.input.keyboard.addKey(Phaser.Keyboard.TWO);
        key2.onDown.addOnce(this.action2, this);

        key3 = game.input.keyboard.addKey(Phaser.Keyboard.THREE);
        key3.onDown.addOnce(this.action3, this);

        key4 = game.input.keyboard.addKey(Phaser.Keyboard.FOUR);
        key4.onDown.addOnce(this.action4, this);

        game.input.keyboard.removeKeyCapture(Phaser.Keyboard.ONE);
        game.input.keyboard.removeKeyCapture(Phaser.Keyboard.TWO);
        game.input.keyboard.removeKeyCapture(Phaser.Keyboard.THREE);
        game.input.keyboard.removeKeyCapture(Phaser.Keyboard.FOUR);

        this.rasp_anim = game.add.image(this.machine.x+90, this.machine.y+270, 'rasp_anim');
        this.rasp_anim.animations.add('raspando');

        this.ac1 = game.add.image(game.world.centerX-50, 270, 'bsf');
        this.ac1.scale.set(0.15);
        this.ac1.pivot.x = 10;
        this.ac1.visible = false;

        this.ac2 = game.add.image(game.world.centerX-50, 270, 'boton');
        this.ac2.scale.set(0.15);
        this.ac2.pivot.x = 10;
        this.ac2.visible = false;

        this.ac3 = game.add.image(game.world.centerX-50, 350, 'dedo');
        this.ac3.scale.set(0.5);
        this.ac3.pivot.x = 10;
        this.ac3.visible = false;

        this.ac4 = game.add.image(game.world.centerX-50, 270, 'herradura');
        this.ac4.scale.set(0.27);
        this.ac4.pivot.x = 5;
        this.ac4.visible = false;

        /*
            Crear los Signos con visible = false
            Si anTime es = 0 Mostrar los signos segun Win/Lose
        */

        efx_rasp = game.add.audio('efx3');
        efx_rasp.volume = 0.5;

        if(ticket.level == 0){
            this.signos1 = game.add.sprite(80, 300, 'signos', 0);
            this.signos1.scale.setTo(.17);
            var cuadroAleatorio = Math.floor(Math.random() * 12);
            this.signos1.frame = cuadroAleatorio;
            this.signos1.cuadro = cuadroAleatorio;
            this.isJugando = false;
            this.signos1.visible = false;

            this.signos2 = game.add.sprite(244, 300, 'signos', 0);
            this.signos2.scale.setTo(.17);
            var cuadroAleatorio = Math.floor(Math.random() * 12);
            this.signos2.frame = cuadroAleatorio;
            this.signos2.cuadro = cuadroAleatorio;
            this.signos2.visible = false;

            this.signos3 = game.add.sprite(410, 300, 'signos', 0);
            this.signos3.scale.setTo(.17);
            var cuadroAleatorio = Math.floor(Math.random() * 12);
            this.signos3.frame = cuadroAleatorio;
            this.signos3.cuadro = cuadroAleatorio;
            this.signos3.visible = false;
        }

        if(ticket.level >= 1){
            this.getRand();
            if(tRandom <= 1){
                this.signos1 = game.add.sprite(80, 300, 'signos', 0);
                this.signos1.scale.setTo(.17);
                var cuadroAleatorio = Math.floor(Math.random() * 12);
                this.signos1.frame = cuadroAleatorio;
                this.signos1.cuadro = cuadroAleatorio;
                this.isJugando = false;
                this.signos1.visible = false;

                this.signos2 = game.add.sprite(244, 300, 'signos', 0);
                this.signos2.scale.setTo(.17);
                this.signos2.frame = cuadroAleatorio;
                this.signos2.cuadro = cuadroAleatorio;
                this.signos2.visible = false;

                this.signos3 = game.add.sprite(410, 300, 'signos', 0);
                this.signos3.scale.setTo(.17);
                this.signos3.frame = cuadroAleatorio;
                this.signos3.cuadro = cuadroAleatorio;
                this.signos3.visible = false;
            } else {
                this.signos1 = game.add.sprite(80, 300, 'signos', 0);
                this.signos1.scale.setTo(.17);
                var cuadroAleatorio = Math.floor(Math.random() * 12);
                this.signos1.frame = cuadroAleatorio;
                this.signos1.cuadro = cuadroAleatorio;
                this.isJugando = false;
                this.signos1.visible = false;

                this.signos2 = game.add.sprite(244, 300, 'signos', 0);
                this.signos2.scale.setTo(.17);
                var cuadroAleatorio = Math.floor(Math.random() * 12);
                this.signos2.frame = cuadroAleatorio;
                this.signos2.cuadro = cuadroAleatorio;
                this.signos2.visible = false;

                this.signos3 = game.add.sprite(410, 300, 'signos', 0);
                this.signos3.scale.setTo(.17);
                var cuadroAleatorio = Math.floor(Math.random() * 12);
                this.signos3.frame = cuadroAleatorio;
                this.signos3.cuadro = cuadroAleatorio;
                this.signos3.visible = false;
            }

            this.rasp_shadow = game.add.image(this.machine.x+90, this.machine.y+270, 'rasp_shadow');
            this.rasp_shadow.animations.add('shadows');
        }

        this.wpop = game.add.sprite(0, 0, 'popup');
        this.wpop.visible = false;

        this.msg = game.add.text(game.world.centerX, game.world.centerY-200, '¡Felicitaciones!');
        this.msg.anchor.set(0.5);
        this.msg.align = 'center';
        this.msg.font = 'Arial Black';
        this.msg.fontSize = 30;
        this.msg.fontWeight = 'bold';
        this.msg.stroke = '#222';
        this.msg.strokeThickness = 6;
        this.msg.fill = '#64ff57';
        this.msg.visible = false;

        this.ipot = game.add.image(game.world.x+360, this.msg.y+50, 'pot');
        this.ipot.visible = false;

        this.text = game.add.text(game.world.centerX, game.world.centerY+200, "Dirigete a Caja para Reclamar tu fabuloso premio");
        this.text.anchor.set(.5);
        this.text.align = 'center';
        this.text.font = 'Arial Black';
        this.text.fontSize = 20;
        this.text.fontWeight = 'bold';
        this.text.stroke = '#0f0f0f';
        this.text.strokeThickness = 3;
        this.text.fill = '#fff';
        this.text.visible = false;

    },

    update: function(){

        if(this.ac1.visible == true){

            this.ac1.x -=4;
            this.ac1.rotation += 0.01;
            amTime -= 1;

        }

        if(this.ac2.visible == true){

            this.ac2.x -=4;
            this.ac2.rotation += 0.01;
            amTime -= 1;
        }

        if(this.ac3.visible == true){

            this.ac3.x -=3;
            this.ac3.rotation -= 0.01;
            amTime -=1;
        }


        if(this.ac4.visible == true){

            this.ac4.x -=3;
            this.ac4.rotation += 0.01;
            amTime -=1;

        }

        if(amTime <= 170)
            this.signos3.visible = true;

        if(amTime <= 140)
            this.signos2.visible = true;

        if(amTime <= 110)
            this.signos1.visible = true;

        if(amTime <= 0){
            sigTime -= 1;

            if(ticket.level == 0){
                if(ticket.numTicket == 0){
                    var wpop = game.add.sprite(0, 0, 'popup');
                    this.losePop();
                } else {
                    if(sigTime <= 0)
                        game.state.start('play');
                }
            } else {
                if(sigTime <= 0){
                    if(count == true){
                        this.wpop.visible = true;
                        this.msg.visible = true;
                        this.ipot.visible = true;
                        this.text.visible = true;
                        winTime -= 1;
                    } else {
                        game.state.start('play');
                    }
                }

                if(winTime <= 0)
                    game.state.start('stand');
            }
        }

    },

    action1: function(){

        this.ac1.visible = true;
        this.rasp_anim.animations.play('raspando', 2, false);
        this.rasp_shadow.animations.play('shadows', 2, false);
        efx_rasp.play();

        //Actualizacion de datos JSON, Jquery AJAX
        $.ajax({
            url: "assets/data/ticket.json",
            type: "PUT",
            data: ticket,
            dataType: "json",
            async: false,
            success: function(data){
                console.log('Action 1, Enviado');
            }
        });
    },

    action2: function(){

        this.ac2.visible = true;
        this.rasp_anim.animations.play('raspando', 2, false);
        this.rasp_shadow.animations.play('shadows', 2, false);
        efx_rasp.play();

        $.ajax({
            url: "assets/data/ticket.json",
            type: "PUT",
            data: ticket,
            dataType: "json",
            async: false,
            success: function(data){
                console.log('Action 2, Enviado');
            }
        });
    },

    action3: function(){

        this.ac3.visible = true;
        this.rasp_anim.animations.play('raspando', 2, false);
        this.rasp_shadow.animations.play('shadows', 2, false);
        efx_rasp.play();

        $.ajax({
            url: "assets/data/ticket.json",
            type: "PUT",
            data: ticket,
            dataType: "json",
            async: false,
            success: function(data){
                console.log('Action 3, Enviado');
            }
        });
    },

    action4: function(){

        this.ac4.visible = true;
        this.rasp_anim.animations.play('raspando', 2, false);
        this.rasp_shadow.animations.play('shadows', 2, false);
        efx_rasp.play();

        $.ajax({
            url: "assets/data/ticket.json",
            type: "PUT",
            data: ticket,
            dataType: "json",
            async: false,
            success: function(data){
                console.log('Action 4, Enviado');
            }
        });
    },

    //Obtener un Numero Random para WIN
    getRand: function(){
        tRandom = Math.floor((Math.random() * ticket.numTicket) + 1);
        if(tRandom <= 1){
            count = true;
        } else {
            count = false;
        }
    },

    //Ventana de Derrota
    losePop: function(){
        var msg = game.add.text(game.world.centerX, game.world.centerY-200, '¡Lo Sentimos!');
        msg.anchor.set(0.5);
        msg.align = 'center';
        msg.font = 'Arial Black';
        msg.fontSize = 30;
        msg.fontWeight = 'bold';
        msg.stroke = '#222';
        msg.strokeThickness = 6;
        msg.fill = '#64ff57';
        msg.alpha = 0.5;

        var ipot = game.add.image(game.world.x+360, msg.y+50, 'pot');
        ipot.alpha = 0.5;

        var text = game.add.text(game.world.centerX, game.world.centerY+200, "Recarga tu suerte y Gana Fabolosos Premios");
        text.anchor.set(.5);
        text.align = 'center';
        text.font = 'Arial Black';
        text.fontSize = 20;
        text.fontWeight = 'bold';
        text.stroke = '#0f0f0f';
        text.strokeThickness = 3;
        text.fill = '#fff';
        text.alpha = 0.5;
    }
};
